'use strict';

import React from 'react';
import { render } from 'react-dom';

import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';

import configureStore from './store/configureStore';
import createHistory from 'history/createBrowserHistory';

import Root from './components/Root';

render(
  <Provider store={configureStore()}>
    <ConnectedRouter history={createHistory()}>
      <Root/>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);
