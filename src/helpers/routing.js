'use strict';

import { last } from 'lodash';

export function getTeamIdFromUrl(url = '') {
  return last(url.split('/'));
}