const baseUrl = 'https://api.football-data.org/v1';
import { FOOTBALL_API_TOKEN } from '../constants';

// accept full and relative urls
export async function callFootballApi(url, options = {}) {
  const resultUrl = url.includes('http') ? url : `${baseUrl}${url}`;

  const response = await fetch(resultUrl.replace(/^http:/, 'https:'), Object.assign(options, {
    headers: {
      'X-Auth-Token': FOOTBALL_API_TOKEN
    }
  }));

  return response.ok ? response.json() : Promise.reject(response.text());
}

export async function callApi(url, options = {}) {
  const response = await fetch(url, options);

  return response.ok ? response.json() : Promise.reject(response.text());
}