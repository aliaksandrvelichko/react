'use strict';

import {
  RECEIVE_STANGINGS,
  REQUEST_STANDINGS
} from '../constants';

export function standings(state = {
  isLoading: false
}, action) {
  switch (action.type) {
    case REQUEST_STANDINGS:
      return { ...state, isLoading: true };
    case RECEIVE_STANGINGS:
      return { ...state, isLoading: false, [action.competitionId]: action.standings };
    default:
      return state;
  }
}
