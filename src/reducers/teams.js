'use strict';

import {
  SHOW_TEAMS_FOR_COMPETITION,
  RECEIVE_TEAMS,
  REQUEST_TEAMS,
  ADD_TEAM_TO_FAVORITE,
  REMOVE_TEAM_FROM_FAVORITE,
  RECEIVE_TEAM_DETAILS
} from '../constants';

export function teams(state = {
  isLoading: false,
  favoriteTeams: []
}, action) {
  switch (action.type) {
    case REQUEST_TEAMS:
      return { ...state, isLoading: true };
    case RECEIVE_TEAMS:
      return { ...state, isLoading: false, [action.competitionId]: action.teams };
    case SHOW_TEAMS_FOR_COMPETITION:
      return {
        ...state,
        activeCompetitionId: action.competitionId,
        selectedCompetition: action.competition
      };
    case RECEIVE_TEAM_DETAILS:
      return { ...state, currentTeamDetails: action.team };
    case ADD_TEAM_TO_FAVORITE:
      return pushFavoriteTeamToState(state, action.team);
    case REMOVE_TEAM_FROM_FAVORITE:
      return removeFavoriteTeamFromState(state, action.team);
    default:
      return state;
  }
}

function pushFavoriteTeamToState(state, team) {
  let returnValue = {};

  if (!state.favoriteTeams.find(elem => elem.name === team.name)) {
    const favoriteTeams = state.favoriteTeams.slice();

    favoriteTeams.push(team);

    returnValue = { ...state, favoriteTeams };
  } else {
    returnValue = state;
  }

  return returnValue;
}

function removeFavoriteTeamFromState(state, team) {
  const favoriteTeams = state.favoriteTeams.slice();
  const index = favoriteTeams.findIndex(elem => elem.name === team.name);

  if (index !== -1) {
    favoriteTeams.splice(index, 1);
    return { ...state, favoriteTeams };
  } else {
    return state;
  }
}