'use strict';

import {
  COMPETITIONS_REQUEST,
  COMPETITIONS_REQUEST_FAILURE,
  COMPETITIONS_REQUEST_SUCCESS,
  SELECT_COMPETITION,
  LEAGUES_TO_SHOW_IN_ORDER
} from '../constants';

export function competitions(state = {
  items: [],
  isLoading: false,
  hasErrored: false
}, action) {
  switch (action.type) {
    case COMPETITIONS_REQUEST:
      return { ...state, isLoading: action.isLoading };
    case COMPETITIONS_REQUEST_SUCCESS:
      return {
        ...state,
        items: LEAGUES_TO_SHOW_IN_ORDER.map(code => {
          return action.items.find(({ league }) => league === code);
        })
      };
    case COMPETITIONS_REQUEST_FAILURE:
      return { ...state, hasErrored: action.hasErrored };
    case SELECT_COMPETITION:
      return {
        ...state,
        selectedCompetitionId: action.selectedCompetitionId,
        selectedCompetition: state.items.find(({ id }) => id.toString() === action.selectedCompetitionId.toString())
      };
    default:
      return state;
  }
}