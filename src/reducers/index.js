'use strict';

import { combineReducers } from 'redux';
import { competitions } from './competitions';
import { standings } from './standings';
import { teams } from './teams';
import { fixtures } from './fixtures';
import { players } from './players';
import { twitter } from './twitter';
import { routerReducer } from 'react-router-redux';

export default combineReducers({
  competitions,
  standings,
  teams,
  fixtures,
  players,
  twitter,
  router: routerReducer
});