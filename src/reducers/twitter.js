'use strict';

import {
  REQUEST_TWITS,
  RECEIVE_TWITS
} from '../constants';

export function twitter(state = {
  twits: [],
  isLoading: false
}, action) {
  switch (action.type) {
    case REQUEST_TWITS:
      return { ...state, isLoading: true };
    case RECEIVE_TWITS:
      return { ...state, twits: action.twits, isLoading: false };
    default:
      return state;
  }
}