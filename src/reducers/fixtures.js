'use strict';

import {
  FIXTURES_REQUEST,
  FIXTURES_REQUEST_FAILURE,
  FIXTURES_REQUEST_SUCCESS,
  FIXTURE_DETAILS_LOADED
} from '../constants';

export function fixtures(state = {
  items: [],
  isLoading: false,
  hasErrored: false
}, action) {
  switch (action.type) {
    case FIXTURES_REQUEST:
      return { ...state, isLoading: action.isLoading };
    case FIXTURES_REQUEST_SUCCESS:
      return { ...state, items: action.items };
    case FIXTURES_REQUEST_FAILURE:
      return { ...state, hasErrored: action.hasErrored };
    case FIXTURE_DETAILS_LOADED:
      return { ...state, fixtureDetails: action.fixtureDetails };
    default:
      return state;
  }
}