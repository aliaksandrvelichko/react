'use strict';

import {
  REQUEST_PLAYERS,
  RECEIVE_PLAYERS,
  SHOW_PLAYERS_FOR_TEAM,
  NUMBER_OF_PLAYERS_TO_SHOW_PAGING
} from '../constants';

export function players(state = {
  items: [],
  isLoading: false,
  hasErrored: false
}, action) {
  switch (action.type) {
    case REQUEST_PLAYERS:
      return { ...state, isLoading: action.isLoading };
    case RECEIVE_PLAYERS:
      return {
        ...state,
        items: action.players,
        showPaging: action.players.length > NUMBER_OF_PLAYERS_TO_SHOW_PAGING
      };
    case SHOW_PLAYERS_FOR_TEAM:
      return { ...state, team: action.team };
    default:
      return state;
  }
}