'use strict';

import { createStore, applyMiddleware } from 'redux';
import reducers from '../reducers';
import createHistory from 'history/createBrowserHistory';

import { routerMiddleware } from 'react-router-redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

const middleware = [
  routerMiddleware(createHistory()),
  logger,
  thunk
];

export default function configureStore() {
  return createStore(
    reducers,
    applyMiddleware(...middleware)
  );
}