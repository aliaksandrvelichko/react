import Select from '../components/Select';
import { connect } from 'react-redux';

import { selectCompetition, fetchCompetitionsIfNeeded } from '../actions';

const mapStateToProps = (state) => {
  return {
    competitions: state.competitions.items
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    selectCompetition: (competitionId) => {
      return dispatch(selectCompetition(competitionId));
    },
    fetchData: () => dispatch(fetchCompetitionsIfNeeded())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Select);