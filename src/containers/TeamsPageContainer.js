import Teams from '../components/Teams';
import { connect } from 'react-redux';

const mapStateToProps = ({ teams }) => {
  return {
    competition: teams.selectedCompetition
  };
};

export default connect(mapStateToProps)(Teams);