import TeamDetails from '../components/TeamDetails';
import { connect } from 'react-redux';

import {
  loadFixtures,
  showPlayersForTeam,
  loadFixtureDetails,
  fetchTeamDetails
} from '../actions/index';

const mapStateToProps = (state) => {
  return {
    fixtures: state.fixtures.items,
    players: state.players.items,
    showPaging: state.players.showPaging,
    fixtureDetails: state.fixtures.fixtureDetails,
    team: state.teams.currentTeamDetails
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadFixtures: (teamId) => dispatch(loadFixtures(teamId)),
    loadPlayers: (teamId) => dispatch(showPlayersForTeam(teamId)),
    loadFixtureDetails: (url) => dispatch(loadFixtureDetails(url)),
    loadTeamDetails: (teamId) => dispatch(fetchTeamDetails(teamId))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TeamDetails);