import Select from '../components/Select';
import { connect } from 'react-redux';

import { fetchCompetitionsIfNeeded, showTeamsForCompetition } from '../actions';

const mapStateToProps = ({ competitions }) => {
  return {
    competitions: competitions.items
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    selectCompetition: (competitionId) => {
      return dispatch(showTeamsForCompetition(competitionId));
    },
    fetchData: () => dispatch(fetchCompetitionsIfNeeded())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Select);