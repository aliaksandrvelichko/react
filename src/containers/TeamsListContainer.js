import { connect } from 'react-redux';
import TeamsList from '../components/TeamsList';
import { addToFavorite } from '../actions';

const mapStateToProps = ({ teams }) => {
  return {
    teams: teams[teams.activeCompetitionId] || [],
    favoriteTeams: teams.favoriteTeams
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToFavorite: (team) => dispatch(addToFavorite(team))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TeamsList);