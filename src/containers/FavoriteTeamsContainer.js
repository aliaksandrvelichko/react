import { connect } from 'react-redux';
import { removeFromFavorite } from '../actions';

import FavoriteTeams from '../components/FavoriteTeams';

const mapStateToProps = (state) => {
  return {
    teams: state.teams.favoriteTeams
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    removeFromFavorite: (team) => dispatch(removeFromFavorite(team))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FavoriteTeams);