import Header from '../components/HeaderNav';
import { connect } from 'react-redux';

const mapStateToProps = ({ router }) => {
  return {
    initialLocation: router.location.pathname
  };
};

export default connect(mapStateToProps)(Header);