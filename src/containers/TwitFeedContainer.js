import TwitFeed from '../components/TwitFeed';
import { connect } from 'react-redux';

import { fetchTwits } from '../actions';

const mapStateToProps = ({ twitter }) => {
  return {
    twits: twitter.twits
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    searchTwits: (text) => dispatch(fetchTwits(text))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TwitFeed);