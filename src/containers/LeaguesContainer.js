import LeagueTable from '../components/LeagueTable';
import { connect } from 'react-redux';

const mapStateToProps = ({ competitions, standings }) => {
  return {
    data: standings[competitions.selectedCompetitionId],
    competition: competitions.selectedCompetition
  };
};

export default connect(mapStateToProps)(LeagueTable);