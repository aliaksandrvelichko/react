import { callFootballApi } from '../helpers';

import {
  FIXTURES_REQUEST,
  FIXTURES_REQUEST_SUCCESS,
  FIXTURES_REQUEST_FAILURE,
  FIXTURE_DETAILS_LOADED
} from '../constants';

export function fixturesIsLoading(isLoading) {
  return {
    type: FIXTURES_REQUEST,
    isLoading
  };
}

export function fixturesHasErrored(bool) {
  return {
    type: FIXTURES_REQUEST_FAILURE,
    hasErrored: bool
  };
}

export function fixturesLoadSuccess(items) {
  return {
    type: FIXTURES_REQUEST_SUCCESS,
    items
  };
}

export function fixtureDetailsLoaded(fixtureDetails) {
  return {
    type: FIXTURE_DETAILS_LOADED,
    fixtureDetails
  };
}

export function loadFixtures(teamId) {
  return async (dispatch) => {
    dispatch(fixturesIsLoading(true));

    try {
      const { fixtures } = await callFootballApi(`/teams/${teamId}/fixtures`);

      dispatch(fixturesIsLoading(false));
      dispatch(fixturesLoadSuccess(fixtures));
    } catch (err) {
      dispatch(fixturesHasErrored(true));
    }
  };
}

export function loadFixtureDetails(url) {
  return async (dispatch) => {
    try {
      const response = await callFootballApi(url);

      dispatch(fixtureDetailsLoaded(response));
    } catch (err) {
      dispatch(fixturesHasErrored(true));
    }
  };
}