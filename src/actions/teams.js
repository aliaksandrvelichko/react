import { callFootballApi } from '../helpers';
import { setSelectedCompetition } from './';

import {
  SHOW_TEAMS_FOR_COMPETITION,
  REQUEST_TEAMS,
  RECEIVE_TEAMS,
  ADD_TEAM_TO_FAVORITE,
  REMOVE_TEAM_FROM_FAVORITE,
  RECEIVE_TEAM_DETAILS
} from '../constants';

export function showTeamsForCompetition(competitionId) {
  return (dispatch, getState) => {
    dispatch(setSelectedCompetition(competitionId));
    dispatch(fetchTeamsIfNeeded(competitionId));

    const { competitions } = getState();

    dispatch({
      type: SHOW_TEAMS_FOR_COMPETITION,
      competitionId,
      competition: competitions.selectedCompetition
    });
  };
}

function requestTeams() {
  return {
    type: REQUEST_TEAMS
  };
}

function receiveTeams(competitionId, teams) {
  return {
    type: RECEIVE_TEAMS,
    competitionId,
    teams
  };
}

function receiveTeamsDetails(info) {
  return {
    type: RECEIVE_TEAM_DETAILS,
    team: info
  };
}

function fetchTeams(competitionId) {
  return async (dispatch) => {
    dispatch(requestTeams(competitionId));

    const { teams } = await callFootballApi(`/competitions/${competitionId}/teams`);

    dispatch(receiveTeams(competitionId, teams));
  };
}

function shouldFetchTeams({ teams }, competitionId) {
  const selectedTeams = teams[competitionId];
  if (!selectedTeams) {
    return true;
  } else if (teams.isLoading) {
    return false;
  }
}

export function fetchTeamsIfNeeded(competitionId) {
  return (dispatch, getState) => {
    if (shouldFetchTeams(getState(), competitionId)) {
      return dispatch(fetchTeams(competitionId));
    }
  };
}

export function fetchTeamDetails(teamId) {
  return async (dispatch) => {
    dispatch(requestTeams());

    const team = await callFootballApi(`/teams/${teamId}`);

    dispatch(receiveTeamsDetails(team));
  };
}

export function addToFavorite(team) {
  return {
    type: ADD_TEAM_TO_FAVORITE,
    team
  };
}

export function removeFromFavorite(team) {
  return {
    type: REMOVE_TEAM_FROM_FAVORITE,
    team
  };
}