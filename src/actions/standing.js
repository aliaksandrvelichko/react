import { callFootballApi } from '../helpers';

import {
  REQUEST_STANDINGS,
  RECEIVE_STANGINGS
} from '../constants';

function requestStandings(competitionId) {
  return {
    type: REQUEST_STANDINGS,
    competitionId
  };
}

function receiveStandings(competitionId, standings) {
  return {
    type: RECEIVE_STANGINGS,
    competitionId,
    standings
  };
}

function fetchStandings(competitionId) {
  return async (dispatch) => {
    dispatch(requestStandings(competitionId));

    const standings = await callFootballApi(`/competitions/${competitionId}/leagueTable`);

    dispatch(receiveStandings(competitionId, standings));
  };
}

function shouldFetchStandings({ standings }, competitionId) {
  const selectedStandings = standings[competitionId];
  if (!selectedStandings) {
    return true;
  } else if (standings.isLoading) {
    return false;
  }
}

export function fetchStandingsIfNeeded(competitionId) {
  return (dispatch, getState) => {
    if (shouldFetchStandings(getState(), competitionId)) {
      return dispatch(fetchStandings(competitionId));
    }
  };
}