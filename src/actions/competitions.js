import { callFootballApi } from '../helpers';
import { fetchStandingsIfNeeded } from './';

import { isEmpty } from 'lodash';

import {
  COMPETITIONS_REQUEST,
  COMPETITIONS_REQUEST_SUCCESS,
  COMPETITIONS_REQUEST_FAILURE,
  SELECT_COMPETITION
} from '../constants';

export function competitionsIsLoading(isLoading) {
  return {
    type: COMPETITIONS_REQUEST,
    isLoading
  };
}

export function competitionsHasErrored(bool) {
  return {
    type: COMPETITIONS_REQUEST_FAILURE,
    hasErrored: bool
  };
}

export function competitionsLoadSuccess(items) {
  return {
    type: COMPETITIONS_REQUEST_SUCCESS,
    items
  };
}

export function setSelectedCompetition(selectedCompetitionId) {
  return {
    type: SELECT_COMPETITION,
    selectedCompetitionId
  };
}

function loadCompetitions() {
  return async (dispatch) => {
    dispatch(competitionsIsLoading(true));

    try {
      const competitions = await callFootballApi('/competitions');

      dispatch(competitionsLoadSuccess(competitions));
    } catch (err) {
      dispatch(competitionsHasErrored(true));
    }
  };
}

export function fetchCompetitionsIfNeeded() {
  return (dispatch, getState) => {
    const { competitions } = getState();

    if (isEmpty(competitions.items)) {
      return dispatch(loadCompetitions());
    }
  };
}

export function selectCompetition(selectedCompetitionId) {
  return (dispatch) => {
    dispatch(fetchStandingsIfNeeded(selectedCompetitionId));
    dispatch(setSelectedCompetition(selectedCompetitionId));
  };
}