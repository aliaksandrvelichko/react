import { callApi } from '../helpers';

import {
  REQUEST_TWITS,
  RECEIVE_TWITS
} from '../constants';

function requestTwits() {
  return {
    type: REQUEST_TWITS
  };
}

function receiveTwits(twits) {
  return {
    type: RECEIVE_TWITS,
    twits
  };
}

export function fetchTwits(searchText) {
  return async (dispatch) => {
    dispatch(requestTwits());

    const twits = await callApi(`/search?text=${searchText}`);

    dispatch(receiveTwits(twits));
  };
}