import { callFootballApi } from '../helpers';

import {
  SHOW_PLAYERS_FOR_TEAM,
  REQUEST_PLAYERS,
  RECEIVE_PLAYERS
} from '../constants';

export function showPlayersForTeam(teamId) {
  return (dispatch, getStore) => {
    dispatch(fetchPlayersIfNeeded(teamId));
    const { teams } = getStore();

    dispatch({
      type: SHOW_PLAYERS_FOR_TEAM,
      team: teams[teamId]
    });
  };
}

function requestPlayers(teamId) {
  return {
    type: REQUEST_PLAYERS,
    teamId
  };
}

function receivePlayers(teamId, players) {
  return {
    type: RECEIVE_PLAYERS,
    teamId,
    players
  };
}

function fetchPlayers(teamId) {
  return async (dispatch) => {
    dispatch(requestPlayers(teamId));

    const { players } = await callFootballApi(`/teams/${teamId}/players`);

    dispatch(receivePlayers(teamId, players));
  };
}

function shouldFetchPlayers({ players }, teamId) {
  const selectedPlayers = Object(players)[teamId];
  if (!selectedPlayers) {
    return true;
  } else if (players.isLoading) {
    return false;
  }
}

export function fetchPlayersIfNeeded(teamId) {
  return (dispatch, getState) => {
    if (shouldFetchPlayers(getState(), teamId)) {
      return dispatch(fetchPlayers(teamId));
    }
  };
}