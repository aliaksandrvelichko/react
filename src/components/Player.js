import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Card } from 'antd';
import moment from 'moment';

class Player extends Component {
  render() {
    const { name, dateOfBirth, position, marketValue } = this.props.player;

    return (
      <Card>
        <div>
          <p>{name}</p>
          <p>Birth date: {moment(dateOfBirth).format('DD.MM.YYYY')}</p>
          <p>Position: {position}</p>
          <p>Market value: {marketValue}</p>
        </div>
      </Card>
    );
  }
}

Player.propTypes = {
  player: PropTypes.shape({
    name: PropTypes.string,
    dateOfBirth: PropTypes.string,
    position: PropTypes.string,
    marketValue: PropTypes.string
  })
};

export default Player;