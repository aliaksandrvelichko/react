import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';
import { Layout, Menu } from 'antd';

const { Header } = Layout;
import { LEAGUES_PATH, TEAMS_PATH } from '../constants/index';

const links = [
  {
    path: LEAGUES_PATH,
    label: 'League table'
  },
  {
    path: TEAMS_PATH,
    label: 'Teams'
  }
];

class HeaderNav extends (Component) {
  render() {
    const defaultSelectedKey = links.findIndex(({ path }) => path === this.props.initialLocation);

    return (
      <Header>
        <Menu
          theme="dark" mode="horizontal"
          style={{ lineHeight: '64px', fontSize: '18px' }}
          defaultSelectedKeys={[`${defaultSelectedKey}`]}>
          {
            links.map(({ path, label }, index) => {
              return (<Menu.Item key={index}>
                <Link to={path}>{label}</Link>
              </Menu.Item>);
            })
          }
        </Menu>
      </Header>
    );
  }
}

HeaderNav.propTypes = {
  initialLocation: PropTypes.string
};

export default HeaderNav;