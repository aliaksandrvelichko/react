import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TeamTile from './TeamTile';
import { Link } from 'react-router-dom';

import { get, toArray } from 'lodash';
import { getTeamIdFromUrl } from '../helpers/index';

class TeamsList extends Component {
  render() {
    return (
      <div>
        {
          this.props.teams.map(team => {
            const teamId = getTeamIdFromUrl(get(team, '_links.self.href'));
            const isFavorited = toArray(this.props.favoriteTeams.map(team => team.name)).includes(team.name);

            return (
              <Link key={teamId} to={`/teams/${teamId}`}>
                <TeamTile team={team} isFavorited={isFavorited} addToFavorite={this.props.addToFavorite}/>
              </Link>
            );
          })
        }
      </div>
    );
  }
}

TeamsList.propTypes = {
  teams: PropTypes.arrayOf(PropTypes.shape({
    crestUrl: PropTypes.string,
    name: PropTypes.string,
    shortName: PropTypes.string,
    squadMarketValue: PropTypes.string
  })),
  favoriteTeams: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string
  })),
  addToFavorite: PropTypes.func.isRequired
};

export default TeamsList;