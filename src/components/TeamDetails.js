import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Tabs } from 'antd';
const TabPane = Tabs.TabPane;

import PlayersList from './PlayersList';
import FixturesPage from './FixturesPage';
import PageTitle from './PageTitle';
import TwitFeed from '../containers/TwitFeedContainer';

class TeamDetails extends Component {
  componentWillMount() {
    const teamId = this.props.match.params.id;
    return Promise.all([
      this.props.loadFixtures(teamId),
      this.props.loadPlayers(teamId),
      this.props.loadTeamDetails(teamId)
    ]);
  }

  render() {
    const { fixtures } = this.props;
    const title = this.props.team ? <PageTitle title={this.props.team.name}/> : null;
    const twitFeed = this.props.team ? <TwitFeed searchText={this.props.team.shortName}/> : null;

    return (
      <section>
        {title}
        <Tabs defaultActiveKey="1">
          <TabPane tab="Info" key="1">
            <div style={{ display: 'flex'}}>
              <div style={{ flex: '1 0 50%', padding: '10px' }}>
                <PlayersList players={this.props.players} showPaging={this.props.showPaging}/>
              </div>
              <div style={{ flex: '1 0 50%', padding: '10px' }}>
                {twitFeed}
              </div>
            </div>
          </TabPane>
          <TabPane tab="Fixtures" key="2">
            <FixturesPage
              fixtures={fixtures}
              loadFixtureDetails={this.props.loadFixtureDetails}
              fixtureDetails={this.props.fixtureDetails}/>
          </TabPane>
        </Tabs>
      </section>
    );
  }
}

TeamDetails.propTypes = {
  players: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    dateOfBirth: PropTypes.string,
    position: PropTypes.string,
    marketValue: PropTypes.string
  })),
  team: PropTypes.shape({
    name: PropTypes.string,
    shortName: PropTypes.string
  }),
  showPaging: PropTypes.bool,
  loadFixtureDetails: PropTypes.func,
  loadPlayers: PropTypes.func,
  loadFixtures: PropTypes.func,
  loadTeamDetails: PropTypes.func,
  fixtureDetails: PropTypes.object
};

export default TeamDetails;