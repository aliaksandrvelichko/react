import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Card } from 'antd';
import moment from 'moment';

class MatchTile extends Component {
  render() {
    const { homeTeamName, awayTeamName, date, result, status } = this.props.match;
    const finishedStatus = 'FINISHED';
    const goalScore = status === finishedStatus ? `${result.goalsHomeTeam} - ${result.goalsAwayTeam}` : '';

    return (
      <div onClick={this.props.onClick}>
        <Card>
          <div>
            <p>{`${homeTeamName} - ${awayTeamName} ${goalScore}`}</p>
            <p>{`${moment(date).utc().format('YYYY-MM-DD HH:mm')} UTC`}</p>
          </div>
        </Card>
      </div>
    );
  }
}

MatchTile.propTypes = {
  match: PropTypes.shape({
    homeTeamName: PropTypes.string,
    awayTeamName: PropTypes.string,
    date: PropTypes.string,
    status: PropTypes.string,
    result: PropTypes.shape({
      goalsHomeTeam: PropTypes.number,
      goalsAwayTeam: PropTypes.number
    })
  }),
  onClick: PropTypes.func
};

export default MatchTile;