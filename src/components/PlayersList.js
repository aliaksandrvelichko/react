import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Pagination } from 'antd';
import Player from './Player';

import { chunk, toArray } from 'lodash';

class PlayersList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pageSize: 5,
      currentPage: 1
    };
  }

  pageChangeHandler = (page) => {
    this.setState(Object.assign({}, this.state, {
      currentPage: page
    }));
  };

  getActiveChunkPlayers = () => {
    let currentPagePlayers;

    if (this.props.showPaging) {
      const chunks = chunk(this.props.players, this.state.pageSize);
      currentPagePlayers = toArray(chunks[this.state.currentPage - 1]); // chunks indexes start from 0, but pagers from 1
    } else {
      currentPagePlayers = this.props.players;
    }

    return currentPagePlayers.map((player, index) => {
      return (<Player key={index} player={player}/>);
    });
  };

  render() {
    const pagerComponent = this.props.showPaging ? (
      <Pagination
        current={this.state.currentPage}
        total={this.props.players.length}
        defaultPageSize={this.state.pageSize}
        onChange={this.pageChangeHandler}/>) : null;
    return (
      <div>
        {pagerComponent}
        <div style={{ padding: '20px 0' }}>
          { this.getActiveChunkPlayers() }
        </div>
        {pagerComponent}
      </div>
    );
  }
}

PlayersList.propTypes = {
  players: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    dateOfBirth: PropTypes.string,
    position: PropTypes.string,
    marketValue: PropTypes.string
  }))
};

export default PlayersList;