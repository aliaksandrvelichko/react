import React from 'react';

import { Route, Redirect } from 'react-router';

import Header from '../containers/HeaderContainer';
import LeaguesContainer from '../containers/LeaguesContainer';
import TeamsPage from '../containers/TeamsPageContainer';
import TeamDetails from '../containers/TeamDetailsContainer';

import 'antd/dist/antd.less';
import { Layout } from 'antd';
const { Content } = Layout;

import { LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';

import { INDEX_PATH, LEAGUES_PATH, TEAMS_PATH, TEAM_DETAILS_PATH } from '../constants';

const Root = () => {
  return (
    <LocaleProvider locale={enUS}>
      <Layout>
        <Header/>
        <Content style={{ padding: '20px', minHeight: 'calc(100vh - 64px)'}}>
          <Route exact strict path={INDEX_PATH} render={() => <Redirect to={LEAGUES_PATH}/>}/>
          <Route exact path={LEAGUES_PATH} component={LeaguesContainer}/>
          <Route exact path={TEAM_DETAILS_PATH} component={TeamDetails}/>
          <Route exact path={TEAMS_PATH} component={TeamsPage}/>
        </Content>
      </Layout>
    </LocaleProvider>
  );
};

export default Root;