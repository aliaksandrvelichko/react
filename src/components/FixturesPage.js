import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Moment from 'moment';
const moment = extendMoment(Moment);
import { extendMoment } from 'moment-range';
import { toArray, get } from 'lodash';

import { DatePicker } from 'antd';
const { RangePicker } = DatePicker;
import MatchTile from './MatchTile';
import FixtureDetails from './FixtureDetails';

class Fixtures extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dateRange: [moment(), moment().add(2, 'week')]
    };
  }

  handleDateChange = (dates) => {
    this.setState({
      dateRange: dates
    });
  };

  loadFixture(url) {
    this.props.loadFixtureDetails(url);
  }

  render() {
    const matches = toArray(this.props.fixtures).filter(fixture => {
      return moment.range(...this.state.dateRange).contains(moment(fixture.date));
    }).map((fixture, index) => {
      return (<MatchTile key={index} match={fixture} onClick={this.loadFixture.bind(this, get(fixture, '_links.self.href'))}/>);
    });

    return (
      <div style={{ display: 'flex'}}>
        <div style={{ flex: '1 0 50%', padding: '10px' }}>
          <RangePicker size="large" defaultValue={this.state.dateRange} onChange={this.handleDateChange} />
          {matches}
        </div>
        <div style={{ flex: '1 0 50%', padding: '10px' }}>
          {
            this.props.fixtureDetails ? <FixtureDetails fixtureDetails={this.props.fixtureDetails}/> : ''
          }
        </div>
      </div>
    );
  }
}

Fixtures.propTypes = {
  fixtureDetails: PropTypes.object,
  fixtures: PropTypes.arrayOf(PropTypes.shape({
    date: PropTypes.string
  })),
  loadFixtureDetails: PropTypes.func
};

export default Fixtures;