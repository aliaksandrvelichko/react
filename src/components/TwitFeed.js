import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Card } from 'antd';
import moment from 'moment';

class TwitFeed extends Component {
  componentWillMount() {
    this.props.searchTwits(this.props.searchText);
  }

  render() {
    return (
      <Card title={`Twitter by #${this.props.searchText}`} noHovering>
        {
          this.props.twits.map(({ created_at, text, user }, index) => {
            return (
              <Card.Grid key={index} style={{ width: '100%' }}>
                <p>{`@${user.screen_name} ${moment(created_at).format('MMMM Do, YYYY HH:mm zz')}`}</p>
                <p>{text}</p>
              </Card.Grid>
            );
          })
        }
      </Card>
    );
  }
}

TwitFeed.propTypes = {
  searchText: PropTypes.string,
  twits: PropTypes.arrayOf(PropTypes.shape({
    created_at: PropTypes.string,
    text: PropTypes.string,
    user: PropTypes.shape({
      screen_name: PropTypes.string
    })
  })),
  searchTwits: PropTypes.func
};

export default TwitFeed;