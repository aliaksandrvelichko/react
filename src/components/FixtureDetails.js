import React, { Component } from 'react';
import PropTypes from 'prop-types';

import moment from 'moment';
import { Card } from 'antd';
import { take } from 'lodash';

import { NUMBER_OF_FIXTURES_TO_DISPLAY } from '../constants/index';

const gridStyle = {
  width: '50%',
  textAlign: 'center'
};

class FixtureDetails extends Component {
  render() {
    const { fixture, head2head } = this.props.fixtureDetails;
    const lastFixtures = take(head2head.fixtures, NUMBER_OF_FIXTURES_TO_DISPLAY);

    return (
      <div>
        <Card title={`${fixture.homeTeamName} - ${fixture.awayTeamName}`}>
          <div>
            <p>{moment(fixture.date).format('MMMM Do, YYYY HH:mm zz')}</p>
            <p>odds</p>
            <p>Head to head: {head2head.count} games</p>
            <p>{fixture.homeTeamName} wins: {head2head.homeTeamWins}</p>
            <p>Draws: {head2head.draws}</p>
            <p>{fixture.awayTeamName} wins: {head2head.awayTeamWins}</p>
          </div>
          <Card>
            {
              lastFixtures.map((fixture, index) => {
                return (
                  <Card.Grid key={index} style={gridStyle}>
                    <div>
                      <p>{fixture.homeTeamName} - {fixture.awayTeamName}
                        <span>{fixture.result.goalsHomeTeam} - {fixture.result.goalsAwayTeam}</span>
                      </p>
                      <p>{moment(fixture.date).format()}</p>
                    </div>
                  </Card.Grid>);
              })
            }
          </Card>
        </Card>
      </div>
    );
  }
}

FixtureDetails.propTypes = {
  fixtureDetails: PropTypes.shape({
    fixture: PropTypes.shape({
      homeTeamName: PropTypes.string,
      awayTeamName: PropTypes.string,
      date: PropTypes.string,
      homeTeamWins: PropTypes.string,
      awayTeamWins: PropTypes.string
    }),
    head2head: PropTypes.shape({
      count: PropTypes.number,
      draws: PropTypes.number,
      fixtures: PropTypes.arrayOf(PropTypes.shape({
        date: PropTypes.string,
        homeTeamName: PropTypes.string,
        awayTeamName: PropTypes.string,
        result: PropTypes.shape({
          goalsHomeTeam: PropTypes.number,
          goalsAwayTeam: PropTypes.number
        })
      }))
    })
  })
};

export default FixtureDetails;