import React from 'react';
import PropTypes from 'prop-types';

import { Select } from 'antd';
const { Option } = Select;

import { head, get } from 'lodash';

class SelectControl extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  async componentWillMount() {
    await this.props.fetchData();
    const selectedCompetitionId = get(head(this.props.competitions), 'id');

    this.selectCompetition(selectedCompetitionId);
  }

  selectCompetition = (value) => {
    this.setState({
      selectedCompetitionId: String(value)
    });

    this.props.selectCompetition(value);
  };

  render() {
    return (
      <Select
        size="large"
        placeholder="Select competition"
        style={{ width: '100%', margin: '20px 0' }}
        onChange={this.selectCompetition}
        value={this.state.selectedCompetitionId}
      >
        {this.props.competitions.map((competition) => {
          return (
            <Option
              value={competition.id.toString()}
              key={competition.id}>
              {competition.caption}
            </Option>
          );
        })}
      </Select>
    );
  }
}

SelectControl.propTypes = {
  competitions: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    caption: PropTypes.string
  })),
  fetchData: PropTypes.func.isRequired,
  selectCompetition: PropTypes.func.isRequired
};

export default SelectControl;