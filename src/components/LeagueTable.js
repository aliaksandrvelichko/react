import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';

import { Link } from 'react-router-dom';
import Select from '../containers/LeagueTableSelect';
import PageTitle from './PageTitle';

import { get } from 'lodash';
import { getTeamIdFromUrl } from '../helpers';

class LeagueTable extends Component {
  constructor(props) {
    super(props);

    this.columns = [
      {
        title: 'Position',
        dataIndex: 'position'
      },
      {
        title: 'Team',
        render: team => {
          const teamId = getTeamIdFromUrl(get(team, '_links.team.href'));
          return (<Link to={`/teams/${teamId}`}>{team.teamName}</Link>);
        }
      },
      {
        title: 'Games',
        dataIndex: 'playedGames'
      },
      {
        title: 'Wins',
        dataIndex: 'wins'
      },
      {
        title: 'Draws',
        dataIndex: 'draws'
      },
      {
        title: 'Losses',
        dataIndex: 'losses'
      },
      {
        title: 'GS',
        dataIndex: 'goals'
      },
      {
        title: 'GC',
        dataIndex: 'goalsAgainst'
      },
      {
        title: 'Points',
        dataIndex: 'points'
      }
    ];
  }

  render() {
    const title = this.props.competition ? <PageTitle title={`${this.props.competition.caption} table`}/> : null;

    return (
      <div>
        {title}
        <Select />
        <Table
          columns={this.columns}
          dataSource={get(this.props.data, 'standing')}
          pagination={false}
          loading={!this.props.data}
        />
      </div>
    );
  }
}

LeagueTable.propTypes = {
  data: PropTypes.shape({
    standing: PropTypes.arrayOf(PropTypes.shape({
      position: PropTypes.number,
      playedGames: PropTypes.number,
      wins: PropTypes.number,
      teamName: PropTypes.string,
      draws: PropTypes.number,
      losses: PropTypes.number,
      goals: PropTypes.number,
      goalsAgainst: PropTypes.number,
      points: PropTypes.number
    }))
  }),
  competition: PropTypes.shape({
    caption: PropTypes.string
  })
};

export default LeagueTable;