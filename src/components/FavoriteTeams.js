'use strict';

import React from 'react';
import { Icon, Card } from 'antd';

class FavoriteTeams extends React.Component {
  render() {
    return (
      <Card title="My favorite teams" noHovering style={{ marginTop: '5px' }}>
        {
          this.props.teams.map(team => {
            return (
              <Card.Grid key={team.name} bordered={false} style={{ padding: '5px', width: '100%' }}>
                <div style={{ display: 'flex', justifyContent: 'space-between', padding: '20px 40px' }}>
                  <p>{team.name}</p>
                  <div onClick={() => this.props.removeFromFavorite(team)}>
                    <Icon type="delete"/>
                  </div>
                </div>
              </Card.Grid>
            );
          })
        }
      </Card>
    );
  }
}

export default FavoriteTeams;