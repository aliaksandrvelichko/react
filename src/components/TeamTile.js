import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Card, Avatar, Icon } from 'antd';

class TeamTile extends Component {
  favoriteHandler = (event) => {
    event.preventDefault();
    this.props.addToFavorite(this.props.team);
  };

  render() {
    const { crestUrl, name, shortName, squadMarketValue } = this.props.team;
    const icon = this.props.isFavorited ? <Icon type="star"/> : <Icon type="star-o"/>;

    return (
      <div style={{ background: '#ECECEC', padding: '5px' }}>
        <Card bordered={false} bodyStyle={{ display: 'flex', justifyContent: 'space-between', zoom: '1.5' }}>
          <Avatar size="large" src={crestUrl}/>
          <div>
            <p>{name}</p>
            <p>Short Name: {shortName}</p>
            <p>Squad Market value: {squadMarketValue}</p>
          </div>
          <div onClick={this.favoriteHandler}>
            {icon}
          </div>
        </Card>
      </div>
    );
  }
}

TeamTile.propTypes = {
  team: PropTypes.shape({
    crestUrl: PropTypes.string,
    name: PropTypes.string,
    shortName: PropTypes.string,
    squadMarketValue: PropTypes.string
  }),
  isFavorited: PropTypes.bool,
  addToFavorite: PropTypes.func
};

export default TeamTile;