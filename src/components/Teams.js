import React from 'react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import Select from '../containers/TeamsSelect';
import TeamsList from '../containers/TeamsListContainer';
import PageTitle from './PageTitle';

import FavoriteTeams from '../containers/FavoriteTeamsContainer';

class TeamsPage extends React.Component {
  render() {
    return (
      <div>
        <PageTitle title={`${get(this.props, 'competition.caption')} table`} />
        <Select/>
        <div style={{ display: 'flex' }}>
          <div style={{ flex: '1 0 50%', padding: '10px' }}>
            <TeamsList/>
          </div>
          <div style={{ flex: '1 0 50%', padding: '10px' }}>
            <FavoriteTeams/>
          </div>
        </div>
      </div>
    );
  }
}

TeamsPage.propTypes = {
  competition: PropTypes.shape({
    caption: PropTypes.string
  })
};

export default TeamsPage;