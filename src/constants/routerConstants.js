export const INDEX_PATH = '';
export const LEAGUES_PATH = '/leagues';
export const TEAMS_PATH = '/teams';
export const TEAM_DETAILS_PATH = '/teams/:id';
