'use strict';

const winston = require('winston');
const config = require('config');

class Logger {
  constructor() {
    this.loggerConfig = config.get('logger');

    this.initialize();
  }

  initialize() {
    const self = this;

    self._logger = new winston.Logger({
      transports: self._getTransports()
    });
  }

  info(message) {
    this._logger.info(message);
  }

  error(message) {
    this._logger.error(message);
  }

  _getTransports() {
    const transports = [];

    this._setConsoleTransport(transports);

    return transports;
  }

  _setConsoleTransport(transports) {
    if (this.loggerConfig.get('transports').indexOf('console') !== -1) {
      transports.push(new winston.transports.Console({
        timestamp: true
      }));
    }
  }
}

module.exports = new Logger();