'use strict';

const Twit = require('twit');
const config = require('config').get('twit');

class TwitterService {
  constructor() {
    this.twit = new Twit({
      consumer_key: config.get('consumerKey'),
      consumer_secret: config.get('consumerSecret'),
      access_token: config.get('accessToken'),
      access_token_secret: config.get('accessTokenSecret'),
      timeout_ms: config.get('timeout')
    });
  }

  searchTweets(text) {
    return new Promise((resolve, reject) => {
      this.twit.get('search/tweets', {
        q: text,
        count: config.get('maxTwitsCount')
      }, ((err, data) => {
          if (err) {
            return reject(err);
          }

          resolve(data.statuses);
        }));
    });
  }
}

module.exports = new TwitterService();