'use strict';

const logger = require('./helpers/logger');
const config = require('config').get('server');
const port = process.env.PORT || config.port;

const Koa = require('koa');
const configApp = require('./appConfiguration');

const app = new Koa();

configApp(app);

app.listen(port, () => {
  logger.info(`Server listening on port ${port}`);
});
