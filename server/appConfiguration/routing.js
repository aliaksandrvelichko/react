'use strict';

const routers = [require('../routes/twit'), require('../routes/static')];

module.exports = (app) => {
  routers.forEach(router => {
    app.use(router.routes()).use(router.allowedMethods());
  })
};