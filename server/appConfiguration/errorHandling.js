'use strict';

module.exports = (app) => {
  app.use(async (ctx, next) => {
    try {
      await next();
    } catch (err) {
      if (err.code && !err.status) {
        err.status = 422;
      }

      ctx.body = err;
      ctx.status = err.status;
    }
  });
};