'use strict';

const config = require('config');
const path = require('path');

const Router = require('koa-router');
const router = new Router();

const send = require('koa-send');

router.get('/index_bundle.js', async (ctx) => {
  const clientDir = config.get('client.directory');
  const root = path.join(process.cwd(), clientDir);

  return send(ctx, 'index_bundle.js', {
    root
  });
});

router.get('/*', async (ctx) => {
  const clientDir = config.get('client.directory');
  const root = path.join(process.cwd(), clientDir);

  return send(ctx, 'index.html', {
    root
  });
});

module.exports = router;