'use strict';

const Router = require('koa-router');
const router = new Router();
const twitterService = require('../services/twit');

router.get('/search', async (ctx) => {
  const { text } = ctx.request.query;
  const data = await twitterService.searchTweets(text);

  ctx.status = 200;
  ctx.body = data;
});

module.exports = router;